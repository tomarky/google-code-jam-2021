import java.io.*;
import java.util.*;

class Solution
{
    static final long[][] TIMES = new long[12*60*60][];

    static void init()
    {
        int i = 0;
        long hh = 0L;
        for (int h = 0; h < 12; h++)
        {
            long mm = 0L;
            for (int m = 0; m < 60; m++)
            {
                long ss = 0L;
                for (int s = 0; s < 60; s++)
                {
                    long[] hms = new long[3];
                    hms[0] = hh;
                    hms[1] = mm;
                    hms[2] = ss;
                    Arrays.sort(hms);
                    hms[1] -= hms[0];
                    hms[2] -= hms[0];
                    hms[0] = 0;
                    TIMES[i] = hms;
                    i++;
                    hh += H;
                    mm += M;
                    ss += S;
                }
            }
        }
    }

    static void solve() throws Exception
    {
        // solution
        long[] ABC = readLongs();
        long A = ABC[0];
        long B = ABC[1];
        long C = ABC[2];

        ABC[0] -= A;
        ABC[1] -= A;
        ABC[2] -= A;

        for (int i = 0; i < TIMES.length; i++)
        {
            if (Arrays.equals(ABC, TIMES[i]))
            {
                int h = i / 60 / 60;
                int m = (i / 60) % 60;
                int s = i % 60;
                out.printf("%d %d %d %d%n", h, m, s, 0);
                return;
            }
        }

        ABC[0] = (A + CYCLE - B) % CYCLE;
        ABC[1] = (B + CYCLE - B) % CYCLE;
        ABC[2] = (C + CYCLE - B) % CYCLE;
        Arrays.sort(ABC);
        for (int i = 0; i < TIMES.length; i++)
        {
            if (Arrays.equals(ABC, TIMES[i]))
            {
                int h = i / 60 / 60;
                int m = (i / 60) % 60;
                int s = i % 60;
                out.printf("%d %d %d %d%n", h, m, s, 0);
                return;
            }
        }

        ABC[0] = (A + CYCLE - C) % CYCLE;
        ABC[1] = (B + CYCLE - C) % CYCLE;
        ABC[2] = (C + CYCLE - C) % CYCLE;
        Arrays.sort(ABC);
        for (int i = 0; i < TIMES.length; i++)
        {
            if (Arrays.equals(ABC, TIMES[i]))
            {
                int h = i / 60 / 60;
                int m = (i / 60) % 60;
                int s = i % 60;
                out.printf("%d %d %d %d%n", h, m, s, 0);
                return;
            }
        }

        out.printf("X %d %d %d %d %n",
            A / A_DEGREE,
            B / A_DEGREE,
            C / A_DEGREE,
            0
        );
    }

    static final long A_DEGREE = 12L * 10_000_000_000L;
    static final long H = 360L * A_DEGREE / (12L * 60L * 60L);
    static final long M = 360L * A_DEGREE / (60L * 60L);
    static final long S = 360L * A_DEGREE / 60L;
    static final long CYCLE = 360L * A_DEGREE;

    static final String IMPOSSIBLE = "IMPOSSIBLE";
    static BufferedReader in;
    static PrintStream out = System.out;

    public static void main(String[] args) throws Exception
    {
        init();

        in = new BufferedReader(new InputStreamReader(System.in));

        int T = readInt();
        for (int i = 1; i <= T; i++)
        {
            out.printf("Case #%d: ", i);
            solve();
        }
    }

    static long[] readLongs() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        long[] ret = new long[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Long.parseLong(tokens[i]);
        }
        return ret;
    }

    static int readInt() throws Exception
    {
        return Integer.parseInt(in.readLine());
    }

    static int[] readInts() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        int[] ret = new int[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Integer.parseInt(tokens[i]);
        }
        return ret;
    }
}