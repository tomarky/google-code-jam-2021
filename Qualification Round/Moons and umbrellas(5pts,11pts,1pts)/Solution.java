import java.io.*;
import java.util.*;

class Solution
{
    static void solve() throws Exception
    {
        // solution
        String[] tokens = in.readLine().split(" ");
        int X = Integer.parseInt(tokens[0]); // CJ
        int Y = Integer.parseInt(tokens[1]); // JC
        char[] S = tokens[2].toCharArray();

        boolean prevC, prevJ;
        int costPrevC = 0, costPrevJ = 0;

        prevC = S[0] != 'J';
        prevJ = S[0] != 'C';

        for (int i = 1; i < S.length; i++)
        {
            if (S[i] == 'C')
            {
                if (prevJ)
                {
                    costPrevC = prevC
                        ? Math.min(costPrevC, costPrevJ + Y)
                        : costPrevJ + Y;
                    prevC = true;
                    prevJ = false;
                }
            }
            else if (S[i] == 'J')
            {
                if (prevC)
                {
                    costPrevJ = prevJ
                        ? Math.min(costPrevC + X, costPrevJ)
                        : costPrevC + X;
                    prevJ = true;
                    prevC = false;
                }
            }
            else
            {
                // S[i] <- 'C'
                int tmpCostPrevC = prevJ
                    ? (prevC
                        ? Math.min(costPrevC, costPrevJ + Y)
                        : costPrevJ + Y)
                    : costPrevC;
                // S[i] <- 'J'
                int tmpCostPrevJ = prevC
                    ? (prevJ
                        ? Math.min(costPrevC + X, costPrevJ)
                        : costPrevC + X)
                    : costPrevJ;

                prevC = true;
                prevJ = true;
                costPrevC = tmpCostPrevC;
                costPrevJ = tmpCostPrevJ;
            }
        }

        int ans = prevC
            ? (prevJ
                ? Math.min(costPrevC, costPrevJ)
                : costPrevC)
            : costPrevJ;

        out.println(ans);
    }

    static final String IMPOSSIBLE = "IMPOSSIBLE";
    static BufferedReader in;
    static PrintStream out = System.out;

    public static void main(String[] args) throws Exception
    {
        in = new BufferedReader(new InputStreamReader(System.in));

        int T = readInt();
        for (int i = 1; i <= T; i++)
        {
            out.printf("Case #%d: ", i);
            solve();
        }
    }

    static int readInt() throws Exception
    {
        return Integer.parseInt(in.readLine());
    }

    static int[] readInts() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        int[] ret = new int[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Integer.parseInt(tokens[i]);
        }
        return ret;
    }
}