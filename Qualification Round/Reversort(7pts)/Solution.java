import java.io.*;
import java.util.*;

class Solution
{
    static void solve() throws Exception
    {
        // solution
        int N = readInt();
        int[] list = readInts();
        int cost = 0;

        for (int i = 0; i < N - 1; i++)
        {
            int min_pos = i;
            for (int p = i; p < N; p++)
            {
                if (list[p] < list[min_pos])
                {
                    min_pos = p;
                }
            }
            cost += min_pos - i + 1;
            for (int p = i; p < min_pos; )
            {
                int t = list[p];
                list[p] = list[min_pos];
                list[min_pos] = t;
                p++;
                min_pos--;
            }
        }

        out.println(cost);
    }

    static final String IMPOSSIBLE = "IMPOSSIBLE";
    static BufferedReader in;
    static PrintStream out = System.out;

    public static void main(String[] args) throws Exception
    {
        in = new BufferedReader(new InputStreamReader(System.in));

        int T = readInt();
        for (int i = 1; i <= T; i++)
        {
            out.printf("Case #%d: ", i);
            solve();
        }
    }

    static int readInt() throws Exception
    {
        return Integer.parseInt(in.readLine());
    }

    static int[] readInts() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        int[] ret = new int[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Integer.parseInt(tokens[i]);
        }
        return ret;
    }
}