import java.io.*;
import java.util.*;

class Solution
{
    static void solve(int N) throws Exception
    {
        // solution
        int[] xs = new int[N];

        for (int i = 0; i < N; i++)
        {
            xs[i] = i + 1;
        }

        for (int i = 2; i < N; i++)
        {
            int lpOut = 0;
            int rpOut = i - 1;
            int lpIn = lpOut;
            int rpIn = rpOut;
            int target = i;
            for (;;)
            {
                out.printf("%d %d %d%n", xs[lpIn], xs[rpIn], xs[i]);
                out.flush();
                int r = readInt();
                if (r < 0) throw new RuntimeException("BAD ANSWER "
                    + lpOut + "," + lpIn + "," + rpIn + "," + rpOut);
                if (r == xs[lpIn])
                {
                    if (lpIn - lpOut <= 1)
                    {
                        target = lpIn;
                        break;
                    }
                    rpOut = lpIn;
                }
                else if (r == xs[rpIn])
                {
                    if (rpOut - rpIn <= 1)
                    {
                        target = rpIn + 1;
                        break;
                    }
                    lpOut = rpIn;
                }
                else if (r == xs[i])
                {
                    if (rpIn - lpIn <= 1)
                    {
                        target = rpIn;
                        break;
                    }
                    lpOut = lpIn;
                    rpOut = rpIn;
                }
                lpIn = lpOut + (rpOut - lpOut) / 3;
                rpIn = Math.max(lpOut + (rpOut - lpOut) / 3 * 2, lpIn + 1);
            }
            int t = xs[i];
            for (int j = i; j > target; j--)
            {
                xs[j] = xs[j - 1];
            }
            xs[target] = t;
        }

        String[] ret = new String[N];
        for (int i = 0; i < N; i++)
        {
            ret[i] = Integer.toString(xs[i]);
        }

        out.println(String.join(" ", ret));
        out.flush();
        if (readInt() < 0) throw new RuntimeException("BAD ANSWER " + Arrays.toString(xs));
    }

    static final String IMPOSSIBLE = "IMPOSSIBLE";
    static BufferedReader in;
    static PrintStream out = System.out;

    public static void main(String[] args) throws Exception
    {
        in = new BufferedReader(new InputStreamReader(System.in));

        int[] TNQ = readInts();
        int T = TNQ[0];
        int N = TNQ[1];
        int Q = TNQ[2];

        for (int i = 1; i <= T; i++)
        {
            solve(N);
        }
    }

    static int readInt() throws Exception
    {
        return Integer.parseInt(in.readLine());
    }

    static int[] readInts() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        int[] ret = new int[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Integer.parseInt(tokens[i]);
        }
        return ret;
    }
}