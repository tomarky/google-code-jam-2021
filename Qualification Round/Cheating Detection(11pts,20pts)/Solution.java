import java.io.*;
import java.util.*;

class Solution
{
    static final boolean DEBUG = false;

    static int solve(byte[][] answers) throws Exception
    {
        // solution

        double[] difficulty = new double[10000];
        double[] skill = new double[100];
        int[] correct = new int[100];
        int[] hardies = new int[100];
        int count = 0;

        for (int q = 0; q < 10000; q++)
        {
            int c = 0;
            for (int i = 0; i < 100; i++)
            {
                c += (int)answers[i][q] - '0';
                correct[i] += (int)answers[i][q] - '0';
            }
            if (c < 20)
            {
                count++;
                for (int i = 0; i < 100; i++)
                {
                    hardies[i] += (int)answers[i][q] - '0';
                }
            }
        }

        Integer[] ranking = new Integer[100];
        for (int i = 0; i < 100; i++)
        {
            ranking[i] = i;
        }
        Arrays.sort(ranking, (a, b) -> Integer.compare(correct[b], correct[a]));

        for (int i = 1; i < 100; i++)
        {
            int a = ranking[i - 1];
            int b = ranking[i];
            hardies[a] -= hardies[b];
        }

        int cheater = 0;

        for (int i = 0; i < 100; i++)
        {
            if (hardies[i] > hardies[cheater])
            {
                cheater = i;
            }
        }

        return cheater + 1;
    }

    static final String IMPOSSIBLE = "IMPOSSIBLE";
    static BufferedReader in;
    static PrintStream out = System.out;

    public static void main(String[] args) throws Exception
    {
        in = new BufferedReader(new InputStreamReader(System.in));

        int T = readInt();
        int P = readInt();
        for (int i = 1; i <= T; i++)
        {
            out.printf("Case #%d: ", i);

            byte[][] answers = new byte[100][];

            for (int p = 0; p < 100; p++)
            {
                answers[p] = in.readLine().getBytes();
            }

            out.println(solve(answers));
        }

        if (DEBUG) check();
    }

    static int readInt() throws Exception
    {
        return Integer.parseInt(in.readLine());
    }

    static int[] readInts() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        int[] ret = new int[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Integer.parseInt(tokens[i]);
        }
        return ret;
    }

    static Random rand = new Random(System.currentTimeMillis());

    static double nextDouble()
    {
        return (double)rand.nextInt(1 << 30) / (double)((1 << 30) - 1);
    }

    static void check() throws Exception
    {
        byte[][] answers = new byte[100][10000];
        double[] skill = new double[100];
        double[] difficulty = new double[10000];
        int[] result = new int[100];

        int correct = 0;

        for (int x = 0; x < 50; x++)
        {
            int cheater = rand.nextInt(100);
            for (int i = 0; i < 100; i++)
            {
                skill[i] = nextDouble() * 6.0 - 3.0;
                result[i] = 0;
            }
            Arrays.sort(skill);
            for (int q = 0; q < 10000; q++)
            {
                difficulty[q] = nextDouble() * 6.0 - 3.0;
            }
            Arrays.sort(difficulty);
            for (int q = 0; q < 10000; q++)
            {
                for (int i = 0; i < 100; i++)
                {
                    if (i == cheater && rand.nextDouble() < 0.5)
                    {
                        answers[i][q] = '1';
                    }
                    else if (rand.nextDouble() < (1.0 / (1.0 + Math.exp(-(skill[i] - difficulty[q])))))
                    {
                        answers[i][q] = '1';
                    }
                    else
                    {
                        answers[i][q] = '0';
                    }
                    result[i] += (int)answers[i][q] - '0';
                }
            }
            for (int i = 0; i < -100; i++)
            {
                out.print(i == cheater ? "* " : "  ");
                out.printf("%4d ", result[i]);
                out.println(new String(answers[i]));
            }
            int ret = solve(answers) - 1;
            if (ret == cheater)
            {
                correct++;
            }
        }
        out.println("correct: " + correct);
        out.println("correct: " + ((double)correct/ 50.0));
    }

}