import java.io.*;
import java.util.*;

class Solution
{
    static void solve() throws Exception
    {
        // solution
        int[] NC = readInts();
        int N = NC[0];
        int C = NC[1];

        if (C < N - 1 || C > N * (N + 1) / 2)
        {
            // out.println(IMPOSSIBLE);
            // return;
        }
        // most cost?
        // 2 3 4 1
        // 2 3 4 5 1
        // 2 3 4 5 6 1
        int[] pos = new int[N];
        String[] ret = new String[N];
        int[] reti = new int[N];

        for (int i = 0; i < N; i++)
        {
            ret[i] = Integer.toString(i + 1);
            reti[i] = i + 1;
        }

        for (int i = 0; i < N - 1; i++)
        {
            int x = Math.min((N - 1) - i + 1, C - (N - 1 - (i + 1)));
            C -= x;
            if (C < 0 || x < 1)
            {
                out.println(IMPOSSIBLE);
                return;
            }
            pos[i] = x + i - 1;
        }

        if (C != 0)
        {
            out.println(IMPOSSIBLE);
            return;
        }

        for (int i = N - 2; i >= 0; i--)
        {
            for (int a = i, b = pos[i]; a < b; a++, b--)
            {
                String t = ret[a];
                ret[a] = ret[b];
                ret[b] = t;

                int ti = reti[a];
                reti[a] = reti[b];
                reti[b] = ti;
            }
        }

        if (NC[1] != solve(N, reti))
        {
            String s = String.format("%d != %d", NC[1], solve(N, reti));
            throw new RuntimeException(s);
        }

        out.println(String.join(" ", ret));
    }

    static int solve(int N, int[] list)
    {
        int cost = 0;

        for (int i = 0; i < N - 1; i++)
        {
            int min_pos = i;
            for (int p = i; p < N; p++)
            {
                if (list[p] < list[min_pos])
                {
                    min_pos = p;
                }
            }
            cost += min_pos - i + 1;
            for (int p = i; p < min_pos; )
            {
                int t = list[p];
                list[p] = list[min_pos];
                list[min_pos] = t;
                p++;
                min_pos--;
            }
        }

        return cost;
    }

    static final String IMPOSSIBLE = "IMPOSSIBLE";
    static BufferedReader in;
    static PrintStream out = System.out;

    public static void main(String[] args) throws Exception
    {
        in = new BufferedReader(new InputStreamReader(System.in));

        int T = readInt();
        for (int i = 1; i <= T; i++)
        {
            out.printf("Case #%d: ", i);
            solve();
        }
    }

    static int readInt() throws Exception
    {
        return Integer.parseInt(in.readLine());
    }

    static int[] readInts() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        int[] ret = new int[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Integer.parseInt(tokens[i]);
        }
        return ret;
    }
}