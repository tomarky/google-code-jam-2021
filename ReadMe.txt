Google Code Jam 2021に参加したときのコード

Qualification Round	2021/03/26-2021/03/28 91pts 685位 (22:55:43)
  Reversort                      7pts
  Moons and umbrellas            5pts+11pts+1pts
  Reversort Engineering          7pts+11pts
  Median Sort                    7pts+11pts+0pts
  Cheating Detection             11pts+20pts
  
https://codingcompetitions.withgoogle.com/codejam/round/000000000043580a
https://codingcompetitions.withgoogle.com/codejam/submissions/000000000043580a/VG9tYXJreQ



Round 1B 2021/04/26 01:00-03:30 11pts 3676位 (1:36:21)
  Broken Clock                   5pts+6pts+0pts
  Subtransmutation               0pts+0pts (read, no idea)
  Digit Blocks                   0pts+0pts (no read)
  
https://codingcompetitions.withgoogle.com/codejam/round/0000000000435baf
https://codingcompetitions.withgoogle.com/codejam/submissions/0000000000435baf/VG9tYXJreQ

